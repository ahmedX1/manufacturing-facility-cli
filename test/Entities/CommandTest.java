/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ben Rouha
 */
public class CommandTest {
    
    public CommandTest() {
    }

    /**
     * Test of manageCommand method, of class Command.
     */
    @Test
    public void testManageCommand() {
        System.out.println("manageCommand");
        String input = "feazf ";
        Command instance = new Command();
        String expResult = "wrong command";
        String result = instance.manageCommand(input);
        assertEquals(expResult, result);
    }
    
}
